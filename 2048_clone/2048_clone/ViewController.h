//
//  ViewController.h
//  2048_clone
//
//  Created by Vikram on 2/6/17.
//  Copyright © 2017 2048_clone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayView.h"
#import <AVFoundation/AVFoundation.h>


@interface ViewController : UIViewController
{
    AVAudioPlayer *audioPlayer;
}
@property (nonatomic) int Score;
@property (nonatomic) int highScore;
@property (nonatomic) int highScoreflashercount;
@end

