//
//  TileView.h
//  2048_clone
//
//  Created by Vikram on 2/8/17.
//  Copyright © 2017 2048_clone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TileView : UIView
@property (nonatomic, strong) UILabel *label;
@property (nonatomic) int value;

-(void)updateLabel;

@end
