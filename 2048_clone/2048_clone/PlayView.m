//
//  PlayView.m
//  2048_clone
//
//  Created by Vikram on 2/8/17.
//  Copyright © 2017 2048_clone. All rights reserved.
//

#import "PlayView.h"

@implementation PlayView
@synthesize directions;
@synthesize tiles;


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



-(int)value:(int)direction row:(int)row col:(int)col
{
    NSArray *a = [directions objectAtIndex:direction];
    a = [a objectAtIndex:row];
   // NSLog(@"Array A has %d elements", (int)[a count]);
    
    for (NSNumber *b in a)
    {
       // NSLog(@"  Contains %@", b);
       
    }
    NSNumber *b = [a objectAtIndex:col];
    
    return (int)[b integerValue];
}

// To make life easier, I'm creating arrays with the indices
// for each of the four possible compression directions.
//
// There's north, south, east, west.  The tile spots are
// numbered 0  1  2  3
//          4  5  6  7
//          8  9 10 11
//         12 13 14 15


-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        tiles = [[NSMutableArray alloc] init];
        CGRect bounds = [self bounds];
        float w = bounds.size.width/4;
        float h = bounds.size.width/4;
              
        for (int r = 0; r < 4; ++r)
        {
            for (int c = 0; c < 4; ++c)
            {
                TileView *tv = [[TileView alloc] initWithFrame:
                    CGRectMake(c*w + 4, r*h + 4, w - 8, h - 8)];
           
                
                [tiles addObject:tv];
                [tv setValue:0];
                [self addSubview:tv];
            }
        }
        
        
        NSArray *up = @[@[@( 0), @( 4), @( 8), @(12)],
                           @[@( 1), @( 5), @( 9), @(13)],
                           @[@( 2), @( 6), @(10), @(14)],
                           @[@( 3), @( 7), @(11), @(15)]];
        NSArray *down = @[@[@(12), @( 8), @( 4), @( 0)],
                           @[@(13), @( 9), @( 5), @( 1)],
                           @[@(14), @(10), @( 6), @( 2)],
                           @[@(15), @(11), @( 7), @( 3)]];
        NSArray *right  = @[@[@( 3), @( 2), @( 1), @( 0)],
                           @[@( 7), @( 6), @( 5), @( 4)],
                           @[@(11), @(10), @( 9), @( 8)],
                           @[@(15), @(14), @(13), @(12)]];
        NSArray *left  = @[@[@( 0), @( 1), @( 2), @( 3)],
                           @[@( 4), @( 5), @( 6), @( 7)],
                           @[@( 8), @( 9), @(10), @(11)],
                           @[@(12), @(13), @(14), @(15)]];
        
        
        directions = @[up, right, left ,down];
    }
    
   
    return self;
}


@end
