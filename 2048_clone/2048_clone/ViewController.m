//
//  ViewController.m
//  2048_clone
//
//  Created by Vikram on 2/6/17.
//  Copyright © 2017 2048_clone. All rights reserved.
//

#import "ViewController.h"



@interface ViewController ()
@property (weak, nonatomic) IBOutlet PlayView *playgame;
@property (weak, nonatomic) IBOutlet UIButton *resetbutton;
@property (weak, nonatomic) IBOutlet UILabel *highscoretaglabel;
@property (weak, nonatomic) IBOutlet UILabel *highscorelabel;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *blurview;

@property (weak, nonatomic) IBOutlet UILabel *blurviewlabel;


@property (weak, nonatomic) IBOutlet UILabel *scorelabel;
@property (weak, nonatomic) IBOutlet UILabel *scoretaglabel;


@property (weak, nonatomic) IBOutlet UIButton *playgamebutton;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.playgamebutton.hidden=YES;
    self.playgame.hidden=YES;
    self.resetbutton.hidden=YES;
    self.scoretaglabel.hidden=YES;
    self.scorelabel.hidden=YES;
    self.highscoretaglabel.hidden=YES;
    self.highscorelabel.hidden=YES;
    self.blurview.hidden=YES;
    _highScoreflashercount=0;
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)playgame:(id)sender {
    self.playgamebutton.hidden=YES;
    self.playgame.hidden=NO;
    self.resetbutton.hidden=NO;
    self.scoretaglabel.hidden=NO;
    self.scorelabel.hidden=NO;
    [self resetGame:0];
    self.scorelabel.text=@"0";
    self.highscoretaglabel.hidden=NO;
    self.highscorelabel.hidden=NO;
     _highScore = [[[NSUserDefaults standardUserDefaults] objectForKey:@"HighScore"] intValue ];
    self.highscorelabel.text=[NSString stringWithFormat:@"%d", _highScore];
    //self.blurview.hidden=NO;
    //self.blurviewlabel.text=@"Blur View Successful...!";
   

}



/*
-(void)gtscore
{
    PlayView *getscore=[[PlayView alloc]init];
    int ScoreValue=0;
    
        ScoreValue=[getscore Score];
        NSLog(@"Score:%d",ScoreValue);
    

}
*/

-(IBAction)tapped:(UIGestureRecognizer *)sender
{
    self.blurview.hidden=YES;
    //NSLog(@"Tapped!");
}

-(IBAction)compress:(UIGestureRecognizer *)sender
{
    int newValues[4];
    int merged[4];
    int count=0;
    NSUInteger nsuint=(NSUInteger)count;
    int scoreincrementer=0;
    
    
    NSArray *compression ;
    UISwipeGestureRecognizerDirection dir=[(UISwipeGestureRecognizer *)sender direction];
    
    switch (dir) {
        case UISwipeGestureRecognizerDirectionRight:
            count=1;
            nsuint=(NSUInteger)count;
            compression = [_playgame.directions objectAtIndex:nsuint];
            //NSLog(@"Swiped Right!");
            break;
            
        case UISwipeGestureRecognizerDirectionLeft:
            count=2;
            nsuint=(NSUInteger)count;
            compression = [_playgame.directions objectAtIndex:nsuint];
            //NSLog(@"Swiped Left!");
            break;
            
        case UISwipeGestureRecognizerDirectionUp:
            count=0;
            nsuint=(NSUInteger)count;
            compression = [_playgame.directions objectAtIndex:nsuint];
            //NSLog(@"Swiped Up!");
            break;
            
        case UISwipeGestureRecognizerDirectionDown:
            count=3;
            nsuint=(NSUInteger)count;
            compression = [_playgame.directions objectAtIndex:nsuint];
            //NSLog(@"Swiped Down!");
            break;
            
        default:
            break;
    }
    
    
    
    for (int row = 0; row < 4; ++row)
    {
        //NSLog(@"Extract row %d", row);
        // Get the indices of the spots we want to compress
        NSArray *thisRow = [compression objectAtIndex:row];
        
        for (int i = 0; i < 4; ++i)
        {
            newValues[i] = 0;
            merged[i] = 0;
        }
        int index = 0;
        
        for (int i = 0; i < 4; ++i)
        {
            NSNumber *n = [thisRow objectAtIndex:i];
            TileView *tv = [_playgame.tiles objectAtIndex:[n integerValue]];
            // NSLog(@"Tile %d has value %d", i, [tv value]);
            if ([tv value] > 0)
            {
                if (index == 0)
                {
                    // NSLog(@"Insert %d", [tv value]);
                    newValues[index++] = [tv value];
                }
                else
                {
                    if ((newValues[index - 1] == [tv value]) && (!merged[index - 1]))
                    {
                        //NSLog(@"Merge to %d", [tv value] * 2);
                        newValues[index - 1] = newValues[index - 1] * 2;
                        merged[index - 1] = 1;
                        scoreincrementer+=newValues[index - 1];                    }
                    else
                    {
                        //NSLog(@"Insert %d", [tv value]);
                        newValues[index++] = [tv value];
                    }
                }
            }
        }
        
        // Fix up the labels!
        for (int i = 0; i < 4; ++i)
        {
            NSNumber *n = [thisRow objectAtIndex:i];
            TileView *tv = [_playgame.tiles objectAtIndex:[n integerValue]];
            [tv setValue:newValues[i]];
            [tv updateLabel];
        }
    }
    
    TileView *tv = [self findEmptyTile];
    if (tv)
    {
        [tv setValue:2];
        [tv updateLabel];
    }
    else
    {
        /*
        self.blurview.hidden=NO;
        self.blurviewlabel.text=@"GAME OVER!";
        _highScoreflashercount++;
        NSString *path = [[NSBundle mainBundle]
                          pathForResource:@"Boo" ofType:@"mp3"];
        audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                       [NSURL fileURLWithPath:path] error:NULL];
        [audioPlayer play];
        //NSLog(@"No empty spot!");
         */
    }
    _Score+=scoreincrementer;
    //NSLog(@"Score: %d",_Score);
    
    self.scorelabel.text=[NSString stringWithFormat:@"%d", _Score];
    
    if(_Score>_highScore)
    {
        if(_highScoreflashercount<1)
        
        {
            self.blurview.hidden=NO;
            self.blurviewlabel.text=@"NEW HIGH SCORE";
            _highScoreflashercount++;
            NSString *path = [[NSBundle mainBundle]
                              pathForResource:@"Applause" ofType:@"mp3"];
            audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                           [NSURL fileURLWithPath:path] error:NULL];
            [audioPlayer play];
        }
        
        _highScore=_Score;
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:_highScore] forKey:@"HighScore"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    self.highscorelabel.text=[NSString stringWithFormat:@"%d", _Score];
    }
    
}

-(TileView *)findEmptyTile
{
    int i = rand() % 16;
    for (int j = 0; j < 15; ++j)
    {
        TileView *tv = [_playgame.tiles objectAtIndex:(i + j)%16];
        if ([tv value] == 0)
            return tv;
    }
    
    return nil;
}

-(IBAction)resetGame:(id)sender
{
    for (int i = 0; i < 16; ++i)
    {
        TileView *tv = [_playgame.tiles objectAtIndex:i];
        [tv setValue:0];
        [tv updateLabel];
    }
    for (int i = 0; i < 3; ++i)
    {
        TileView *tv = [self findEmptyTile];
        if (tv)
        {
            [tv setValue:2];
            [tv updateLabel];
        }
    }
}


@end
