//
//  TileView.m
//  2048_clone
//
//  Created by Vikram on 2/8/17.
//  Copyright © 2017 2048_clone. All rights reserved.
//

#import "TileView.h"

@implementation TileView
@synthesize label;
@synthesize value;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.width)];
        [self addSubview:label];
        [label setText:@"---"];
        [label setTextAlignment:NSTextAlignmentCenter];
        [self setBackgroundColor:[UIColor grayColor]];
        
    }
    return self;
}

-(void)updateLabel
{
    if (value == 0){
        [label setText:@""];
        [label setBackgroundColor:[UIColor lightGrayColor]];    }
    else
        if(value == 2){
        [label setText:[NSString stringWithFormat:@"%d", value]];
        [label setBackgroundColor:[UIColor lightGrayColor]];
        }
    else
        if(value == 4){
                [label setText:[NSString stringWithFormat:@"%d", value]];
                [label setBackgroundColor:[UIColor grayColor]];
            }
    else
        if(value == 8){
                    [label setText:[NSString stringWithFormat:@"%d", value]];
                    [label setBackgroundColor:[UIColor darkGrayColor]];
                }
    else
        if(value == 16){
                        [label setText:[NSString stringWithFormat:@"%d", value]];
                        [label setBackgroundColor:[UIColor greenColor]];
                    }
    else
        if(value == 32){
                            [label setText:[NSString stringWithFormat:@"%d", value]];
                            [label setBackgroundColor:[UIColor cyanColor]];
                        }
    else
        if(value == 64){
                                [label setText:[NSString stringWithFormat:@"%d", value]];
                                [label setBackgroundColor:[UIColor redColor]];
                            }
    else
        if(value == 128){
                [label setText:[NSString stringWithFormat:@"%d", value]];
                [label setBackgroundColor:[UIColor redColor]];
            }
    else
        if(value == 256){
                [label setText:[NSString stringWithFormat:@"%d", value]];
                [label setBackgroundColor:[UIColor redColor]];
            }
    else
        if(value == 512){
                    [label setText:[NSString stringWithFormat:@"%d", value]];
                    [label setBackgroundColor:[UIColor redColor]];
                }
    else
        if(value == 1024){
                        [label setText:[NSString stringWithFormat:@"%d", value]];
                        [label setBackgroundColor:[UIColor redColor]];
                    }
    else
        if(value == 2048){
                            [label setText:[NSString stringWithFormat:@"%d", value]];
                            [label setBackgroundColor:[UIColor redColor]];
                        }
    else
        [label setText:[NSString stringWithFormat:@"%d", value]];
    
    
}
@end
