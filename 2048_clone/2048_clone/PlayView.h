//
//  PlayView.h
//  2048_clone
//
//  Created by Vikram on 2/8/17.
//  Copyright © 2017 2048_clone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TileView.h"

@interface PlayView : UIView
@property (nonatomic, strong) NSArray *directions;
@property (nonatomic, strong) NSMutableArray *tiles;



@end
